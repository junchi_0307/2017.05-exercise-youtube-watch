@echo off

FOR /L %%G IN (1,1,1000) DO (
	
	ipconfig/flushdns>nul
	ipconfig/release>nul
	ipconfig/renew>nul
	echo IP is reset

	echo open youtube %%G
	node youtube-watch.js
)

echo STOP Play
pause